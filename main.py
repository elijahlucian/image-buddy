import PIL.Image
import sys 
import os

def main(img):
    img = PIL.Image.open(img)
    exif = img._getexif()
    # https://stackoverflow.com/questions/4764932/in-python-how-do-i-read-the-exif-data-for-an-image

    # now open up the CSV file you are working with, 
    # append a new row to the CSV
    
    
# call our program with a specific image address 
main('img.jpg') 

# or use sys.argv 
# https://stackoverflow.com/questions/4033723/how-do-i-access-command-line-arguments
main(sys.argv[1]) 

# or get fancy
# pass in a folder name to this program
# get all the files in the folder 
files = os.listdir(sys.argv[1]) # pretend this is a bunch of your image files

for file in files:
    main(file)
    # boom